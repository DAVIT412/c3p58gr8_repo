from django.conf.urls import url
from django.urls.resolvers import URLPattern
from EmployeApp import views

urlpatterns=[

    url(r'^UsuarioInventario$',views.UsuarioRestApi),
    url(r'^UsuarioInventario/([0-9]+)$',views.UsuarioRestApi)

]