from django.db import models

# Create your models here.

class UsuarioInventario (models.Model):
    UsuarioId = models.AutoField(primary_key=True)
    UsuarioNombre = models.CharField(max_length=180)
    UsuarioContraseña = models.CharField(max_length=20)
    UsuarioOrganizacion = models.CharField(max_length=40)
    UsuarioNit = models.CharField(max_length=10)
    UsuarioGenero = models.CharField(max_length=15)
    UsuarioTelefono= models.CharField(max_length=12)
    
