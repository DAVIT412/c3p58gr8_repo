from rest_framework import serializers
from EmployeApp.models import UsuarioInventario

class Usuario_Serializers(serializers.ModelSerializer):
    class Meta: 
        model = UsuarioInventario
        fields=('UsuarioId','UsuarioNombre','UsuarioContraseña','UsuarioOrganizacion','UsuarioNit','UsuarioGenero','UsuarioTelefono')
