from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from EmployeApp.serializers import Usuario_Serializers
from EmployeApp.models import UsuarioInventario



# Create your views here.
@csrf_exempt 
def UsuarioRestApi(request, id=0):
    if request.method == 'GET':
        usuarioinventario = UsuarioInventario.objects.all()
        usuarioserializers = Usuario_Serializers(usuarioinventario,many=True)
        return JsonResponse(usuarioserializers.data,safe=False)

    elif request.method == 'POST':
        usuario_data = JSONParser().parse(request)
        UsuarioSerializers = Usuario_Serializers(data=usuario_data)
        if UsuarioSerializers.is_valid():
            UsuarioSerializers.save()
            return JsonResponse("Se Agrego Correcto 1",safe=False)
        return JsonResponse("No se agrego Correcto 1",safe=False)
    elif request.method == 'PUT':
        usuarioData = JSONParser().parse(request)
        usuarioinventario = UsuarioInventario.objects.get(UsuarioId=usuarioData['UsuarioId'])
        usuarioserializers = Usuario_Serializers(usuarioinventario,data=usuarioData)
        if usuarioserializers.is_valid():
            usuarioserializers.save()
            return JsonResponse("Actualizado",safe=False)
        return JsonResponse("No Actualizado",safe=False)
    elif request.method =='DELETE':
        usuarioinventario = UsuarioInventario.objects.get(UsuarioId=id)
        usuarioinventario.delete()
        return JsonResponse("Elimino", safe=False)